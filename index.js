const express = require('express')
const path = require('path')
const sqlite3 = require('sqlite3').verbose();

// création du serveur express
const app = express();

// Configuration du serveur 
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({extended: false}));

//  test config serveur  
app.listen(3000, () => {
    console.log('Serveur démarré (http://localhost:3000/) !');
})

// GET /
app.get('/', (req, res) => {
    res.render("index")
})

// GET /about
app.get('/about', (req, res) => {
    res.render("about");
});


// GET /data
app.get('/data', (req, res) => {
    const test = {
        titre : 'Test',
        items : ["un", "deux", "trois"]
    };
    res.render("data", {model: test});
})

// GET /student
app.get('/student', (req, res) => {
    const sql = "SELECT * FROM student ORDER BY lastname";
    db.all(sql, [], (err, rows) => {
        if(err) {
            return console.error(err.message)
        }
        res.render('student', {model: rows});
    })
})

//  GET /edit
app.get('/edit/:id', (req, res) => {
    const id = req.params.id;
    const sql = "SELECT * FROM student WHERE id_student = ?";
    db.get(sql, id, (err, row) => {
        if(err) {
            return console.error(err.message);
        }
        console.log(row)
        res.render("edit", {model: row});
    })
})

// POST /edit/
app.post('/edit/:id', (req, res) => {
    const id = req.params.id;
    const student = [req.body.lastname, req.body.firstname, req.body.numberId,req.body.birthdate, id];
    const sql = "UPDATE student SET lastname = ?, firstname = ?, numberId = ?, birthdate = ? WHERE (id_student = ?)";
    db.run(sql, student, err => {
        if(err) {
            return console.error(err.message);
        }
        res.redirect("/student");
    })
})

// GET /create
app.get("/create", (req, res) => {
    res.render('create', {model : {}});
})

// POST /create
app.post('/create', (req, res) => {
    const sql = "INSERT INTO student (lastname, firstname, numberId, birthdate) VALUES (?, ?, ?, ?)";
    const student = [req.body.lastname, req.body.firstname, req.body.numberId, req.body.birthdate];
    db.run(sql, student, err => {
        if(err) {
            return console.error(err.message);
        }
        res.redirect('/student')
    })
})


// GET /delete
app.get("/delete/:id", (req, res) => {
    const id = req.params.id;
    const sql = "SELECT * FROM student id_student = ?";
    db.get(sql, id, (err, row) => {
      if(err) {
        console.error(err.message);
      }
      res.render("delete", { model: row });
    });
  });

// // POST /delete/
// app.post("/delete/:id", (req, res) => {
//     const id = req.params.id;
//     const sql = "DELETE FROM student WHERE id_student = ?";
//     db.run(sql, id, err => {
//       if(err) {
//         console.error(err.message);
//       }
//       res.redirect("/student");
//     });
//   });


const db_name = path.join(__dirname, "data", "student.db");
const db = new sqlite3.Database(db_name, err => {
    if(err) {
        return console.error(err.message);
    }
    console.log("Connexion réussit à la base de données student.db")
})

const sqlite_create_table = `CREATE TABLE IF NOT EXISTS student(
    id_student INTEGER PRIMARY KEY AUTOINCREMENT,
    lastname VARCHAR(50) NOT NULL, 
    firstname VARCHAR(50) NOT NULL,
    numberId INTEGER,
    birthdate DATE
);`;

db.run(sqlite_create_table, err => {
    if(err) {
        return console.error(err.message);
    }
    console.log("Création réussit de la table student")

});

//  Alimentation de la table

// const sql_insert = `INSERT INTO student(id_student, lastname, firstname, commentaires) VALUES
// (1, 'TEJ', 'Yassine','blablabla'),
// (2, 'AAAAA', 'Salem','blablabla'),
// (3, 'LOUARTANI-TEJ', 'Adam','blablablabla');`;
// db.run(sql_insert, err => {
//     if(err) {
//         return console.error(err.message);
//     }
//     console.log("Alimentation réussit de la table student");
// });